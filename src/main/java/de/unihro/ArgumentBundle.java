package de.unihro;

/**
 * Created by xor on 3/13/17.
 */
class ArgumentBundle {
    public int[] measurements = new int[]{400, 800};
    public int sensors = 20;
    public float rangeTop = 5.0f;
    public float rangeBottom = -2.0f;
    public int seed = 1;
    public String user = "postgres";
    public String password = "";

    public String url = "postgres";
}
