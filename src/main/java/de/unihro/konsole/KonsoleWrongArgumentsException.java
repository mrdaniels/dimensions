package de.unihro.konsole;

/**
 * Created by xor on 3/12/17.
 */
public class KonsoleWrongArgumentsException extends Exception {
    public KonsoleWrongArgumentsException(String msg) {
        super(msg);
    }
}
