package de.unihro;

public class LeTimer {

    private String name;
    private long sum;
    private long startTime;
    private long fps = 0;
    private long startCount = 0;
    private boolean mute;

    public LeTimer(String name) {
        this.name = name;
    }

    public LeTimer start() {
        startTime = System.nanoTime();
        startCount++;
        return this;
    }

    public long fps() {
        long duration = (System.currentTimeMillis() - fps);
        fps = System.currentTimeMillis();
        if (duration > 0) {
            return 1000 / duration;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Timer[" + name + "]: " + getDurationInMS() + "ms";
    }

    public LeTimer stop() {
        sum = System.nanoTime() - startTime + sum;
        return this;
    }

    public long getDurationInMS() {
        return (sum / 1000000);
    }

    public long getDurationInNS() {
        return sum;
    }

    public long getDurationInS() {
        return ((System.nanoTime() - startTime) / 1000000000);
    }

    public LeTimer print() {
        if (!mute && startCount > 0) {
            long total = sum / 1000000;
            long avg = total / startCount;
            System.out.println(this.getClass().getSimpleName() + ".'" + name + "'.print avg:" + avg + " total:" + total);
        } else if (!mute)
            System.out.println(this.getClass().getSimpleName() + ".'" + name + "'.print never started");
        return this;
    }

    public LeTimer reset() {
        sum = 0;
        startCount = 0;
        return this;
    }

    public long getStartCount() {
        return startCount;
    }

    public long getAverageDuration() {
        return sum / startCount;
    }

    public LeTimer mute() {
        this.mute = true;
        return this;
    }

    public LeTimer out(String s) {
        if (!mute)
            System.out.println(this.getClass().getSimpleName() + ".'" + name + "'.out: " + s);
        return this;
    }
}

