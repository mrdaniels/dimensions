package de.unihro;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * Created by xor on 2/10/17.
 */
public class TestDataCreator {
    private final Random random;
    private final String filename;
    private final int measurements, sensors;
    private final float rangeBottom, rangeTop;
    private final long seed;
    private StringBuilder b;
    private int lineCount = 0;
    private FileWriter fileWriter;
    private LineCreator lineCreator;
    private LineSeparator lineSeparator;


    public TestDataCreator(int measurements, int sensors, float rangeBottom, float rangeTop, long seed, String filename) {
        assert rangeTop > rangeBottom;
        this.measurements = measurements;
        this.sensors = sensors;
        this.rangeBottom = (rangeBottom < 0) ? (-1 * rangeBottom) : rangeBottom;
        this.rangeTop = rangeTop;
        this.seed = seed;
        this.filename = filename;
        this.random = new Random(seed);
        this.b = new StringBuilder();
    }

    public static void main(String... args) throws IOException {
        TestDataCreator testDataCreator = new TestDataCreator(2000000, 15, -2.0f, 5.0f, 1L, "testdata.csv");
        testDataCreator.csv();
        System.out.println("DONE!");
    }

    public void csv(String delim) throws IOException {
        if (delim != null)
            lineCreator = (i, ii, value) -> b.append(i).append(",").append(ii).append(",").append(value).append(delim);
        else
            lineCreator = (i, ii, value) -> b.append(i).append(",").append(ii).append(",").append(value).append("\n");
        createFile(null);
        generate();
        closeFile(b.toString());
    }

    private void closeFile() throws IOException {
        closeFile(null);
    }

    private void generate() throws IOException {
        for (int i = 0; i < measurements; i++) {
            for (int ii = 0; ii < sensors; ii++) {
                Float value = random.nextFloat();
                if (random.nextBoolean()) { // make value negative
                    value = -value;
                    value *= rangeBottom;
                } else {
                    value *= rangeTop;
                }
                lineCreator.createLine(i, ii, value);
                lineCount++;
                checkWrite();
                if ((i < measurements - 1 || ii < sensors - 1) && lineSeparator != null)
                    lineSeparator.createSeparator();
            }
        }
    }

    public void sql() throws IOException {
        lineCreator = (i, ii, value) -> {
            b.append("(").append(i).append(",").append(ii).append(",").append(value).append(")");
        };
        lineSeparator = () -> b.append(",\n");
        createFile("drop table if exists import;\n" +
                "CREATE TABLE if not exists  import  (\n" +
                "   Messung  int NOT NULL,\n" +
                "   Sensor  int NOT NULL,\n" +
                "   Wert  double precision NOT NULL,\n" +
                "   primary key (messung,sensor),\n" +
                "   UNIQUE (messung,sensor)\n" +
                ");\n" +
                "\n" +
                "--\n" +
                "-- Daten für Tabelle  import \n" +
                "--\n" +
                "\n" +
                "INSERT INTO  import  ( Messung ,  Sensor ,  Wert ) VALUES\n");

        generate();
        closeFile(";");
    }

    private void closeFile(String s) throws IOException {
        if (s != null)
            fileWriter.append(s);
        fileWriter.flush();
        fileWriter.close();
    }

    private void createFile(String s) throws IOException {
        fileWriter = new FileWriter(filename);
        if (s != null)
            fileWriter.append(s);
        fileWriter.flush();
    }

    private void checkWrite() throws IOException {
        lineCount++;
        if (lineCount == 2000) {
            fileWriter.append(b.toString());
            b = new StringBuilder();
            lineCount = 0;
        }
    }

    private void csv() throws IOException {
        csv(null);
    }

    private interface LineCreator {
        void createLine(int i, int ii, float value);
    }

    private interface LineSeparator {
        void createSeparator();
    }


}
