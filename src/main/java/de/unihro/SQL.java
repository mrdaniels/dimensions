package de.unihro;

import de.unihro.konsole.Konsole;
import de.unihro.konsole.KonsoleWrongArgumentsException;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Created by xor on 2/16/17.
 */
@SuppressWarnings("Duplicates")
public class SQL {
    private static final String CSV_FILE_NAME = "jdbc.test.csv";
    private static final String SQL_PREPARE = "/prepare.sql";
    private static final String SQL_RUN = "/runall.sql";
    private final int sensors;
    private final float rangeBottom;
    private final float rangeTop;
    private final int seed;
    private final int[] measurements;
    private final String user, password;
    private final String url;
    private Connection con;
    private PreparedStatement pComputeGamma, pComputeBeta, pTempTable, pComputeP, pComputePAP;

    public SQL(ArgumentBundle b) {
        this.measurements = b.measurements;
        this.sensors = b.sensors;
        this.rangeTop = b.rangeTop;
        this.rangeBottom = b.rangeBottom;
        this.seed = b.seed;
        this.user = b.user;
        this.password = b.password;
        this.url = b.url;
    }

    /**
     * an example: -m 500 -ranget 5.0f -rangeb -2.0f -seed 1 -sensors 20
     *
     * @param args
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws KonsoleWrongArgumentsException
     */
    public static void main(String... args) throws IOException, SQLException, ClassNotFoundException, KonsoleWrongArgumentsException {

        ArgumentBundle b = new ArgumentBundle();
        Konsole konsole = new Konsole();
        konsole.addDefinition("-m", (args1, pos) -> {
            List<Integer> list = new ArrayList<>();
            int count = 0;
            while (pos < args1.length) {
                try {
                    Integer n = Integer.parseInt(args1[pos]);
                    count++;
                    list.add(n);
                    pos++;
                } catch (Exception e) {
                    break;
                }
            }
            b.measurements = list.stream().mapToInt(n -> n).toArray();
            return count;
        }).addDefinition("-ranget", (args1, pos) -> {
            b.rangeTop = Float.parseFloat(args1[pos]);
            return 1;
        }).addDefinition("-rangeb", (args1, pos) -> {
            b.rangeBottom = Float.parseFloat(args1[pos]);
            return 1;
        }).addDefinition("-seed", (args1, pos) -> {
            b.seed = Integer.parseInt(args1[pos]);
            return 1;
        }).addDefinition("-sensors", (args1, pos) -> {
            b.sensors = Integer.parseInt(args1[pos]);
            return 1;
        }).addDefinition("-url", (args1, pos) -> {
            b.url = args1[pos];
            if (!b.url.startsWith("//"))
                System.out.println("argument \"url\" should probably start with //");
            return 1;
        }).addDefinition("-user", (args1, pos) -> {
            b.user = args1[pos];
            return 1;
        }).addDefinition("-password", (args1, pos) -> {
            b.password = args1[pos];
            return 1;
        });

        konsole.handle(args);
        SQL sql = new SQL(b);
        sql.runPerfTestsTests();
    }

    public SQL connect() throws ClassNotFoundException, SQLException {
        if (con == null) {
            Class.forName("org.postgresql.Driver");
            Properties props = new Properties();
            props.setProperty("user", user);
            props.setProperty("password", password);
            this.con = DriverManager.getConnection("jdbc:postgresql:" + url, props);
        }
        return this;
    }

    private void prepareTest(int measurements) throws IOException, SQLException, ClassNotFoundException {
        LeTimer timer = new LeTimer("default").mute().start();
        //System.out.println("SQL.main.start.matrixHeight = " + matrixHeight);
        TestDataCreator testDataCreator = new TestDataCreator(measurements, sensors, rangeBottom, rangeTop, seed, CSV_FILE_NAME);
        testDataCreator.csv(null);
        timer.stop().print().reset();
        rollback();
        timer.out("SQL.main.prep...");
        timer.start();
        prepareSql();
        timer.stop().print().reset();
        timer.out("SQL.main.import...");
        timer.start();
        importCSV();
        timer.stop().print().reset();
        timer.out("SQL.main.run...");
    }

    private void runPerfTestsTests() throws IOException, SQLException, ClassNotFoundException {
        connect();
        for (int matrixHeight : measurements) {
            System.out.println("testing with " + matrixHeight + " measurements");
            LeTimer tAll = new LeTimer("runAlltogether...");
            LeTimer tRough = new LeTimer("runSeperatedRoughly...");
            LeTimer tFine = new LeTimer("runSeperatedFine...");

            //todo runAlltogether (all sql) or runSeperatedRoughly or runSeperatedFine
//            prepareTest(matrixHeight);
//            tAll.start();
//            runAlltogether();
//            tAll.stop();

            prepareTest(matrixHeight);
            tRough.start();
            runSeperatedRoughly();
            tRough.stop();

//            prepareTest(matrixHeight);
//            tFine.start();
//            runSeperatedFine();
//            tFine.stop();
//            tAll.print();
            tRough.print();
//            tFine.print();
            System.out.println();
        }
    }

    private void runSeperatedRoughly() throws IOException, SQLException {
        LeTimer t1 = new LeTimer("rough.copy to daten");
        LeTimer t2 = new LeTimer("rough.update daten");
        LeTimer t3 = new LeTimer("rough.insert into messungen");
        LeTimer t4 = new LeTimer("rough.hessenberg");
        List<LeTimer> timers = Arrays.asList(t1, t2, t3, t4);
        for (int i = 0; i < timers.size(); i++) {
            LeTimer timer = timers.get(i);
            String sql = readFileResource("/run" + i + ".sql");
            Statement statement = con.createStatement();
            timer.start();
            statement.execute(sql);
            timer.stop();
        }
        timers.forEach(LeTimer::print);
    }

    private void runSeperatedFine() throws IOException, SQLException {
        LeTimer t1 = new LeTimer("fine.copy to daten");
        LeTimer t2 = new LeTimer("fine.update daten");
        LeTimer t3 = new LeTimer("fine.insert into messungen");
        List<LeTimer> timers = new ArrayList<>();
        timers.addAll(Arrays.asList(t1, t2, t3));
        for (int i = 0; i < timers.size(); i++) {
            LeTimer timer = timers.get(i);
            String sql = readFileResource("/run" + i + ".sql");
            Statement statement = con.createStatement();
            timer.start();
            statement.execute(sql);
            statement.close();
            timer.stop();
        }
        //hessenberg
        LeTimer hOverall = new LeTimer("fine.overall");
        LeTimer hWhile = new LeTimer("fine.while");
        LeTimer hGamma = new LeTimer("fine.gamma");
        LeTimer hBeta = new LeTimer("fine.beta");
        LeTimer hTmpTbl = new LeTimer("fine.tmp.table");
        LeTimer hComputeP = new LeTimer("fine.computep");
        LeTimer hComputePAP = new LeTimer("fine.computepap");
        hOverall.start();
        int n = 0, k = 1;
        double gamma = 0, beta = 1;
        {
            //  SELECT COUNT(DISTINCT Sensor) INTO n FROM messungen;
            n = hesseCount();
            while (k <= n - 2) {
//                gamma = computeGamma(k);
//                beta = computeBeta(k, gamma);
//                PERFORM prepareTempTbl(n);
//                PERFORM computeP(k, beta);
//                PERFORM computePAP();
//                k = k + 1;
                hGamma.start();
                gamma = hesseGamma(k);
                hGamma.stop();
                hBeta.start();
                beta = hesseBeta(k, gamma);
                hBeta.stop();
                hTmpTbl.start();
                hesseTempTable(n);
                hTmpTbl.stop();
                hComputeP.start();
                hesseComputeP(k, beta);
                hComputeP.stop();
                hComputePAP.start();
                hesseComputePAP();
                hComputePAP.stop();
                k++;
            }
        }

        hOverall.stop();
        timers.addAll(Arrays.asList(hGamma, hBeta, hTmpTbl, hComputeP, hComputePAP));
        timers.forEach(LeTimer::print);
    }

    private void hesseComputePAP() throws SQLException {
        if (pComputePAP == null) {
            pComputePAP = con.prepareCall("SELECT computePAP();");
        }
        pComputePAP.execute();
    }

    private void hesseComputeP(int k, double beta) throws SQLException {
        if (pComputeP == null) {
            pComputeP = con.prepareCall("SELECT computeP(?,?);");
        }
        pComputeP.clearParameters();
        pComputeP.setObject(1, k);
        pComputeP.setObject(2, beta);
        pComputeP.execute();
    }

    private void hesseTempTable(int n) throws SQLException {
        if (pTempTable == null) {
            pTempTable = con.prepareCall("SELECT prepareTempTbl(?);");
        }
        //pTempTable.clearParameters();
        pTempTable.setObject(1, n);
        pTempTable.execute();
    }

    private double hesseBeta(int k, double gamma) throws SQLException {
        if (pComputeBeta == null) {
            pComputeBeta = con.prepareCall("SELECT computeBeta(?, ?);");
        }
        pComputeBeta.clearParameters();
        pComputeBeta.setObject(1, k);
        pComputeBeta.setObject(2, gamma);
        pComputeBeta.execute();
        ResultSet result = pComputeBeta.getResultSet();
        result.next();
        return result.getDouble(1);
    }

    private double hesseGamma(int k) throws SQLException {
        if (pComputeGamma == null) {
            pComputeGamma = con.prepareCall("SELECT computeGamma(?);");
        }
        pComputeGamma.clearParameters();
        pComputeGamma.setObject(1, k);
        pComputeGamma.execute();
        ResultSet result = pComputeGamma.getResultSet();
        result.next();
        return result.getDouble(1);
    }

    private int hesseCount() throws SQLException {
        Statement st = con.createStatement();
        st.execute("SELECT COUNT(DISTINCT Sensor) FROM messungen;");
        ResultSet resultSet = st.getResultSet();
        resultSet.next();
        int n = resultSet.getInt(1);
        st.close();
        return n;
    }

    private void rollback() throws SQLException {
        Statement statement = con.createStatement();
        statement.execute("ROLLBACK;");
    }

    private void runAlltogether() throws IOException, SQLException {
        Statement statement = con.createStatement();
        statement.execute(readFileResource(SQL_RUN));
    }

    private String readFileResource(String res) throws IOException {
        File f = new File(getClass().getResource(res).getFile());
        byte[] bytes = Files.readAllBytes(Paths.get(f.toURI()));
        return new String(bytes);
    }

    private void prepareSql() throws IOException, SQLException {
        Statement statement = con.createStatement();
        statement.execute(readFileResource(SQL_PREPARE));
    }

    private void importCSV() throws IOException, SQLException {
        File csv = new File(CSV_FILE_NAME);
        BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(csv), 512);
        CopyManager copyManager = new CopyManager((BaseConnection) con);
        String sql = "copy import from STDIN delimiter ',' CSV;";
        copyManager.copyIn(sql, inputStream);
    }
}
