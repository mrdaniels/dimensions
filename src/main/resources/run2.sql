INSERT INTO messungen SELECT
                        A.sensor,
                        B.sensor                                      AS Sensor,
                        SUM(A.wert * B.wert) / (COUNT(A.messung) - 1) AS Wert
                      FROM
                        daten A INNER JOIN daten B ON (A.messung = B.messung)
                      GROUP BY A.sensor, B.sensor;