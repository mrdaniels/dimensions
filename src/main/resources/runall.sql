DO $$ BEGIN

  -- Vorbereitung 1.) Die ersten 225 Zeilen (entspricht 15x15 Matrix) aus Tabelle import in Tabelle "messungen" inserten
  -- Tabelle messungen sollte vorher natürlich geleert sein ...
  INSERT INTO daten (messung, sensor, wert)
    SELECT
      A.messung + 1,
      A.sensor + 1,
      A.wert
    FROM import A;
  --   WHERE (messung < 40000);

  -- 1. Schritt) Mittelwerte der Daten berechnen und diese von den Messungen abziehen
  --UPDATE messungen A
  --INNER JOIN (SELECT Sensor, AVG(Wert) AS Mittelwert  FROM messungen GROUP BY Sensor) B ON (A.sensor = B.sensor)
  --SET A.wert = A.wert - B.mittelwert;

  -- postgres port
  UPDATE daten AS A
  SET wert = wert - B.mittelwert
  FROM (SELECT
          sensor,
          avg(wert) AS mittelwert
        FROM daten
        GROUP BY sensor) AS B
  WHERE A.sensor = B.sensor;

  -- 2. Schritt Kovarianzmatrix bilden
  INSERT INTO messungen SELECT
                          A.sensor,
                          B.sensor                                      AS Sensor,
                          SUM(A.wert * B.wert) / (COUNT(A.messung) - 1) AS Wert
                        FROM
                          daten A INNER JOIN daten B ON (A.messung = B.messung)
                        GROUP BY A.sensor, B.sensor;

  -- 3. Schritt Prozedur durchlaufen lassen
  --CALL transformHessenberg();
  PERFORM transformHessenberg();
END; $$ LANGUAGE plpgsql;