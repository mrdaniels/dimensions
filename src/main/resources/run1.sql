UPDATE daten AS A
SET wert = wert - B.mittelwert
FROM (SELECT
        sensor,
        avg(wert) AS mittelwert
      FROM daten
      GROUP BY sensor) AS B
WHERE A.sensor = B.sensor;
COMMIT;