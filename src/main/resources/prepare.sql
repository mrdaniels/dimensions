DROP TABLE IF EXISTS messungen;
DROP TABLE IF EXISTS import;
DROP TABLE IF EXISTS daten;
CREATE TABLE import (
  messung INTEGER          NOT NULL,
  sensor  INTEGER          NOT NULL,
  wert    DOUBLE PRECISION NOT NULL,
  CONSTRAINT import_pkey PRIMARY KEY (messung, sensor)
);
CREATE TABLE messungen (
  Messung INT              NOT NULL,
  Sensor  INT              NOT NULL,
  Wert    DOUBLE PRECISION NOT NULL,
  PRIMARY KEY (Messung, Sensor)
);
CREATE TABLE IF NOT EXISTS daten (
  messung INTEGER,
  sensor  INTEGER,
  wert    DOUBLE PRECISION,
  PRIMARY KEY (Messung, Sensor)

);

CREATE OR REPLACE FUNCTION computeBeta(IN k INT, IN gamma DOUBLE PRECISION)
  RETURNS DOUBLE PRECISION AS $$
DECLARE beta DOUBLE PRECISION := 1;
BEGIN
  SELECT 1 / (gamma * (gamma + abs(Wert)))
  INTO beta
  FROM messungen
  WHERE (sensor = k AND messung = k + 1);
  RETURN beta;
END;$$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION computeGamma(IN k INT)
  RETURNS DOUBLE PRECISION AS $$
DECLARE gamma DOUBLE PRECISION := 0;
BEGIN
  SELECT (SELECT sqrt(sum(power(wert, 2)))
          FROM messungen
          WHERE (messung >= k + 1 AND sensor = k))
  INTO gamma;
  RETURN gamma;
END;$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION prepareTempTbl(IN n INT)
  RETURNS VOID AS $$
DECLARE   k INT DEFAULT 1;
  DECLARE i INT DEFAULT 1;
BEGIN
  DROP TABLE IF EXISTS P;
  CREATE TABLE IF NOT EXISTS P (LIKE messungen INCLUDING ALL
  ); --SELECT * FROM messungen LIMIT 0;
  --ALTER TABLE P ADD PRIMARY KEY( Messung, Sensor);
  TRUNCATE TABLE P;
  WHILE k <= n LOOP
    i = 1;
    WHILE i <= n LOOP
      IF i = k
      THEN
        INSERT INTO P (messung, sensor, wert)
        VALUES (k, k, 1);
      ELSE
        INSERT INTO P (messung, sensor, wert)
        VALUES (k, i, 0);
      END IF;
      i = i + 1;
    END LOOP;
    k = k + 1;
  END LOOP;
END;$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION computeP(IN k INT, IN beta DOUBLE PRECISION)
  RETURNS VOID AS $$
BEGIN
  INSERT INTO P (messung, sensor, wert)
    SELECT
      B.messung,
      C.messung                                        AS Sensor,
      (-beta * ((B.wert + B.vec) * (C.wert + C.vec2))) AS Wert
    FROM (
           SELECT
             A.messung,
             A.sensor,
             A.wert,
             CASE
             WHEN messung = (k + 1)
               THEN sign(wert) * (SELECT sqrt(sum(power(wert, 2)))
                                  FROM messungen
                                  WHERE (messung >= (k + 1) AND sensor = k))
             ELSE
               0
             END AS vec
           FROM messungen A
           WHERE (A.messung >= (k + 1) AND A.sensor = k))
      AS B
      , (
          SELECT
            A.messung,
            A.sensor,
            A.wert,
            CASE
            WHEN messung = (k + 1)
              THEN sign(wert) * (SELECT sqrt(sum(power(wert, 2)))
                                 FROM messungen
                                 WHERE (messung >= (k + 1) AND sensor = k))
            ELSE
              0
            END AS vec2
          FROM messungen A
          WHERE (A.messung >= (k + 1) AND A.sensor = k))
        AS C
  ON CONFLICT (messung, sensor)
    DO UPDATE SET Wert = (P.wert + excluded.wert);
END;$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION computePAP()
  RETURNS VOID AS $$
BEGIN
  INSERT INTO messungen (messung, sensor, wert)
    SELECT *
    FROM (
           SELECT
             C.messung,
             D.sensor,
             SUM(C.wert * D.wert) AS wert
           FROM (
                  SELECT
                    A.messung,
                    B.sensor,
                    SUM(A.wert * B.wert) AS wert
                  FROM P A
                    INNER JOIN messungen B ON (A.sensor = B.Messung)
                  GROUP BY A.Messung, B.Sensor
                )
             AS C
             INNER JOIN P D ON (C.sensor = D.Messung)
           GROUP BY C.Messung, D.Sensor) AS E
  ON CONFLICT (messung, sensor)
    DO UPDATE SET Wert = excluded.wert;
END;$$ LANGUAGE plpgsql;

--drop function transformHessenberg();
CREATE OR REPLACE FUNCTION transformHessenberg()
  RETURNS VOID AS $$
DECLARE   n     INT DEFAULT 0;
  DECLARE k     INT DEFAULT 1;
  DECLARE gamma DOUBLE PRECISION DEFAULT 0;
  DECLARE beta  DOUBLE PRECISION DEFAULT 1;
BEGIN
  -- n bestimmen (Anzahl der Sensoren)
  SELECT COUNT(DISTINCT Sensor)
  INTO n
  FROM messungen;
  -- Schleife 1..n-2
  WHILE k <= (n - 2) LOOP
    gamma = computeGamma(k);
    beta = computeBeta(k, gamma);
    PERFORM prepareTempTbl(n);
    PERFORM computeP(k, beta);
    PERFORM computePAP();
    k = k + 1;
  END LOOP;
  --SELECT * from messungen;
END;$$ LANGUAGE plpgsql;